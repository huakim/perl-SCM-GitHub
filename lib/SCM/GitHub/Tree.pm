#!/usr/bin/perl

package SCM::GitHub::Tree;

#use Moose;
#extends
use base 'SCM::GitTree';

use SCM::Registry::Get;
use JSON::PP;
use SCM::GitHub::Repo;
use Scalar::Util 'blessed';
use experimental 'try';

sub new{
  my $pkg = shift;
  my $github = shift;
  if (not blessed($github)){
    if (not $github->isa('SCM::GitHub::Repo')){
      $github = SCM::GitHub::Repo->new($github, shift, shift);
    }
  }
  my $ref = {'scm' => $github, 'dir' => {}};
  bless($ref, $pkg);
  return $ref;
}


sub convert{
  my ($self, $hash) = @_;
  my $name = $hash->{'path'};
  my $type = $hash->{'type'};
  my $ref = {};
  my $pkg;
  my $github = $self->{'scm'};
  $ref->{'parent'} = $self;
  my $path = $self->{'path'};
  if ($path){
    $path = $path . '/' . $name;
  } else {
    $path = $name;
  }
  if ($type eq 'tree'){
    $ref->{'url'} = $hash->{'url'};
    goto ret2;
  }
  if ($type eq 'commit'){
    my $commit = $hash->{'sha'};
    $github = $github->getsubmodule($path, $commit);
    return $github->getroottree();
  #  $path = '';
  #  goto ret2;
  }
  if ($type eq 'blob'){
    $pkg = 'SCM::Blob';
    $ref->{'size'} = $hash->{'size'};
    goto ret1;
  }
  goto ret3;
  ret2:
  $pkg = 'SCM::GitHub::Tree';
  $ref->{'dir'} = {};
  ret1:
  $ref->{'scm'} = $github;
  $ref->{'path'} = $path;
  bless($ref, $pkg);
  return $ref;
  ret3:
}

sub getlist{
  my $self = shift;
  my $list = $self->{'list'};
  if (not defined($list)){
    $list = {};
    $self->{'list'} = $list;
    my $url = $self->{'url'};
    if (not defined ($url)){
      my $github = $self->{'scm'};
      my $owner = $github->{'owner'};
      my $repo = $github->{'repo'};
      my $commit = $github->{'commit'};
      $url = "https://api.github.com/repos/$owner/$repo/git/trees/$commit";
      $self->{'url'} = $url;
    }
    try{
      my $conf = SCM::Registry::Get::curl($url);

      $conf = decode_json($conf);
      $conf = $conf->{'tree'};
      for (@$conf){
        $list->{$_->{'path'}} = $_;
      }
    } catch ($v){

    }
  }
  return $list;
}

1;
