#!/usr/bin/perl
package SCM::GitHub::Repo;
#use Moose;
#extends
use base 'SCM::GitRepo';

use SCM::GitHub::Tree;
use SCM::Registry::Get;
use Config::Tiny;
use experimental 'try';
use JSON::PP;
use URI;

sub new{
  my ($pkg, $owner, $repo, $commit) = @_;
  my $ref = {'owner' => $owner, 'repo' => $repo, 'commit' => $commit};
  bless($ref, $pkg);
  return $ref;
}

sub getroottree{
  my $github = shift;
  my $ref = {'scm' => $github, 'dir' => {}};
  bless($ref, 'SCM::GitHub::Tree');
  return $ref;
}

sub gettarballprefix{
  my $self = shift;
  my $ssha = $self->getlastshortsha();
  my $repo = $self->{'repo'};
  my $owner = $self->{'owner'};
  return "$owner-$repo-$ssha";
}

sub gettarballurl{
  my $self = shift;
  my $sha = $self->getlastsha();
  my $owner = $self->{'owner'};
  my $repo = $self->{'repo'};
  my $ssha = $self->getlastshortsha();
  return "https://github.com/$owner/$repo/tarball/$sha/#/$owner-$repo-$ssha.tar.gz";
}

#use Data::Dumper;

sub getlastcommit{
  my $self = shift;
  my $ret = $self->{'lastcommit'};
  if (not defined($ret)){
    my $url = $self->getlastcommiturl();
  #  print("$url\n");
    try{
      $ret = decode_json(SCM::Registry::Get::curl($url));
    } catch ($v){
      $ret = {};
    }
  #  print(Dumper($ret));
    $self->{'lastcommit'} = $ret;
  }
  return $ret;
}


sub getlastshortsha{
  my $self = shift;
  my $sha = $self->getlastsha();
  if (length($sha) > 7){
    return substr($sha, 0, 7);
  } else {
    return $sha;
  }
}

sub getlastsha{
  my $self = shift;
  my $com = $self->getlastcommit();
  my $sha = $com->{'sha'};
  if (not defined($sha)){
    $com->{'sha'} = '';
    return '';
  }
  return $sha;
}

sub getlasttagname{
  my $self = shift;
  my $tags = $self->gettags();
  if (ref($tags) eq 'ARRAY' and scalar @$tags){
    return $tags->[0]->{'name'};
  } else {
    return "";
  }
}

sub gettags{
  my $self = shift;
  my $ret = $self->{'taglist'};
  if (not defined($ret)){
    lab1:
    my $arr = [];

    my $owner = $self->{'owner'};
    my $repo = $self->{'repo'};
    my $url = "https://api.github.com/repos/$owner/$repo/tags";
    try{
      $ret = decode_json(SCM::Registry::Get::curl($url));
    } catch ($v){
      $ret = [];
    }
    if (scalar @$ret){
      $arr = [@$ret, @$arr];
      goto lab1;
    } else {
      $ret = $arr;
    }
    $self->{'taglist'} = $ret;
  }
  return $ret;
}

sub getlastcommiturl{
  my $self = shift;
  my $owner = $self->{'owner'};
  my $repo = $self->{'repo'};
  my $commit = $self->{'commit'};
  return "https://api.github.com/repos/$owner/$repo/commits/$commit";
}

sub getcontenturl{
  my ($self, $path) = @_;
  my $owner = $self->{'owner'};
  my $repo = $self->{'repo'};
  my $commit = $self->{'commit'};
  return "https://raw.githubusercontent.com/$owner/$repo/$commit/$path";
}

sub createrepo{
  my ($pkg, $url, $gitpath, $commit) = @_;
  $url = URI->new($url)->path();
  my @values = $url =~ '/+([^/]+)/+([^/]+?)(?:\.git)?$';
  my $github = {
   'owner' => $values[0],
   'repo' => $values[1],
   'commit' => $commit,
   'path' => $gitpath
  };
  bless($github, $pkg);
  return $github;
}

1;
