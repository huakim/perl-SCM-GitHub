#!/usr/bin/perl

package SCM::Tree;
#use Moose;
use SCM::Path;
use base 'SCM::Path';

#use LWP::Simple qw();
#use LWP::Protocol::https;
#use SCM::Registry::Get;

use JSON::PP;
use Data::Dumper;
use Scalar::Util 'blessed';
use experimental 'try';

sub getbyname{
}

sub getlist{
}

sub getsubmodulenames{
  my $self = shift;
  my $repo = $self->getrepo();
  my $names = $repo->getsubmodulenames();
  my $gitpath = $repo->{'path'};
  my $path = $self->{'path'};
  my $str;
  if ($path){
    my @parts = grep { /\S/ } split(/\//, $path);
    my $str = '../' x (scalar @parts);
    if ($gitpath) { $str = "$gitpath/$str" ; };
  }
  if ($str){
    $names = [map { "$str$_" } @$names];
  }
  return $names;
}

sub getsize{
  my $self = shift;
  my $size = $self->{'size'};
  if (not defined($size)){
    my $list = $self->getlist();
    $size = keys %$list;
    $self->{'size'} = $size;
  }
  return $size;
}

#=head
sub get{
  my ($self, $path) = @_;
  my @parts = grep { /\S/ } split(/\//, $path);
  my $cond = 1;
  goto lb1;
  lb2:
    if ($path eq "."){
      goto lb1;
    }
    if ($path eq ".."){
      $cond = $self->{'parent'};
      if ($cond) {$self = $cond; };
      goto lb1;
    }
    $self = $self->getbyname($path);
    if ($cond = blessed($self)) {
      $cond = $self->isa('SCM::Tree');
    };
    lb1:
    $path = shift @parts;
    if (not defined($path)){
      return $self;
    }
  if ($cond) { goto lb2; };
}
#=cut
1;
