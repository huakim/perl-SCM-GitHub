#!/usr/bin/perl

package SCM::GitTree;
use base 'SCM::Tree';

#use Data::Dumper;

sub getbyname{
  my ($self, $name) = @_;
  my $dir = $self->{'dir'};
  my $ret = $dir->{$name};
#  print("NAME* $name\n");
  if (not defined($ret)){
    my $list = $self->getlist();
  #  print(Dumper($list));
    $list = $list->{$name};
    if (defined ($list)){
      $ret = $self->convert($list);
    } else {
      $ret = '';
    }
    $dir->{$name} = $ret;
  }
  if ($ret){
    return $ret;
  }
}

sub getnames{
  my $self = shift;
  my $values = $self->{'names'};
  if (not defined($values)){
    my $list = $self->getlist();
    $values = [keys %$list];
    $self->{'names'} = $values;
  }
  return $values;
}

1;
