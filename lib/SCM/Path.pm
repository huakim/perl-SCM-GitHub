#!/usr/bin/perl

package SCM::Path;

#use Moose;


sub getrepo{
  return $_[0]->{'scm'};
}

sub fullpath{
  my $self = shift;
  my $repo = $self->getrepo();
  my $gitpath = $repo->{'path'};
  my $path = $self->{'path'};
  if (not $path){
     return $gitpath;
  }
  if ($gitpath){
    $path = "$gitpath/$path";
  }
  return $path;
}
#
# sub getpath{
#   my $self = shift;
#   my $github = $self->{'scm'};
#   my $gitpath = $github->{'path'};
#   my $path = $self->{'path'};
#   if (not $gitpath){
#     return $path;
#   } else {
#     if (not $path){
#       return $gitpath;
#     } else {
#       return $gitpath . '/' . $path;
#     }
#   }
# }
1;
