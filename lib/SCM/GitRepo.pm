package SCM::GitRepo;
use base 'SCM::Repo';
use SCM::Registry::Get;
use experimental 'try';


sub getsubmodule{
  no warnings;
  my ($self, $path, $commit) = @_;
  my $submodules = $self->{'submodules'};
  my $coms;
  my $github;
  if (not defined($submodules)){
    $submodules = {};
    $self->{'submodules'} = $submodules;
    loopback:
    $coms = {};
    $submodules->{$path} = $coms;
  } else {
    $coms = $submodules->{$path};
    if (not defined($coms)){
      goto loopback;
    } else {
      $github = $coms->{$commit};
    }
  }
  if (not defined($github)){
    my $list = $self->getlist();
    my $url = $list->{$path};
    if (defined($url)){
      $url = $url->{'url'};
      if (defined($url)){
        #my @values = $url =~ 'https?://github\.com/([^/]+)/([^/]+?)(?:\.git)?$';
        #if (scalar @values == 2){
        my $gitpath = $self->{'path'};
        if ($gitpath){
          $gitpath = $gitpath . '/' . $path;
        } else {
          $gitpath = $path;
        }
        my $host = URI->new($url);
        $github = SCM::Registry::Get::open($host, $gitpath, $commit);
      }
    }
    if (not defined($github)){
      $github = '';
    }
    $coms->{$commit} = $github;
  }
  if ($github){
    return $github;
  }
}

sub getcontent{
  my ($self, $path, $url) = @_;
  my $contentlist = $self->{'contents'};
  my $content = '';
  if (not defined($contentlist)){
    $contentlist = {};
    $self->{'contents'} = $contentlist;
    goto ret1;
  } else {
    $content = $contentlist->{$path};
    if (not defined($content)){
      goto ret1;
    }
  }
  goto ret2;
  ret1:
  try {
    if (not defined ($url)){
      $url = $self->getcontenturl($path);
    }
    $content = SCM::Registry::Get::curl($url);
  } catch($v) {
  }
  $contentlist->{$path} = $content;
  ret2:
  return $content;
}

sub getsubmodulenames{
  my $self = shift;
  my $list = $self->getlist();
  my $values = [keys %$list];
  return $values;
}


sub getlist{
  my $self = shift;
  my $list = $self->{'list'};
  if (not defined($list)){
    $list = {};
    $self->{'list'} = $list;
    try{
      my $conf = $self->getcontent(".gitmodules");
      $conf = Config::Tiny->read_string($conf);
      for (values %$conf){
        $list->{$_->{'path'}} = $_;
      }
    } catch ($v){
    }
  }
  return $list;
}
1;
