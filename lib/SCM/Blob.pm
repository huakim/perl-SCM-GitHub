#!/usr/bin/perl
package SCM::Blob;
#use Moose;
use SCM::Path;
use base 'SCM::Path';

sub new{
  my ($pkg, $github, $path, $url) = @_;
  my $ref = {'scm', $github, 'path', $path, 'url', $url};
  bless($ref, $pkg);
  return $ref;
}

sub getsize{
  my $self = shift;
  my $size = $self->{'size'};
  if (not defined($size)){
    $content = $self->getcontent();
    $size = length($content);
    $self->{'size'} = $size;
  }
  return $size;
}

sub getcontent{
  my $self = shift;
  my $content = $self->{'content'};
  if (not defined($content)){
    $content = $self->{'scm'}->getcontent($self->{'path'}, $self->{'url'});
    $self->{'content'} = $content;
  }
  return $content;
}

1;
