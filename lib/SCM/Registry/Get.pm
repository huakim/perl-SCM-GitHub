package SCM::Registry::Get;
use File::ShareDir 'module_dir';
use File::Slurper 'read_text';
use Module::Load;
use File::Spec;
use experimental 'try';

use LWP::Protocol::https;
use HTTP::CookieJar::LWP ();
use LWP::UserAgent       ();

my $ua  = LWP::UserAgent->new(
    cookie_jar        => HTTP::CookieJar::LWP->new(),
    protocols_allowed => ['http', 'https'],
    timeout           => 10,
    agent             => 'Mozilla/5.0'
);

my $dirpath = module_dir('SCM::Registry::Get');

my %dict;

sub curl{
  my $url = shift;
  return $ua->get($url)->content();
}

sub open{
  my $uri = $_[0];
  my $host = $uri->host();
  my $scm = $dict{$host};
  if (not defined($scm)){
    my $domain = File::Spec->catfile($dirpath, "$host.txt");
    if (-f $domain){
      $scm = read_text($domain);
      $scm =~ s/^\s+|\s+$//g;
      try{
        load $scm;
      } catch ($v){
        $scm = '';
      }
    } else {
      $scm = '';
    }
    $dict{$host} = $scm;
  }
  if ($scm){
    return $scm->createrepo(@_);
  }

}

1;
