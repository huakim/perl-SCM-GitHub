#!/usr/bin/perl
package SCM::GitLab::Repo;
#use Moose;
#extends
use base 'SCM::GitRepo';

use SCM::GitLab::Tree;
use SCM::Registry::Get;
use Config::Tiny;
#use LWP::Simple qw();
use experimental 'try';
use JSON::PP;
use URI;

sub new{
  my ($pkg, $owner, $repo, $commit, $domain) = @_;
  if (not defined($domain)){
    $domain = 'https://gitlab.com';
  }
  my $ref = {'owner' => $owner, 'repo' => $repo, 'commit' => $commit, 'domain' => $domain};
  bless($ref, $pkg);
  return $ref;
}

sub getroottree{
  my $github = shift;
  my $ref = {'scm' => $github, 'dir' => {}};
  bless($ref, 'SCM::GitLab::Tree');
  return $ref;
}

sub gettarballprefix{
  my $self = shift;
  my $ssha = $self->getlastsha();
  my $repo = $self->{'repo'};
  my $owner = $self->{'owner'};
  return "$repo-$ssha";
}

sub gettarballurl{
  my $self = shift;
  my $domain = $self->{'domain'};
  my $sha = $self->getlastsha();
  my $owner = $self->{'owner'};
  my $repo = $self->{'repo'};
  return "$domain/$owner/$repo/-/archive/$sha/$repo-$sha.tar.gz";
}

sub getlastsha{
  my $self = shift;
  my $com = $self->getlastcommit();
  my $sha = $com->{'id'};
  if (not defined($sha)){
    $com->{'id'} = '';
    return '';
  }
  return $sha;
}

sub getlasttagname{
  my $self = shift;
  my $tags = $self->gettags();
  if (scalar @$tags){
    return $tags->[0]->{'name'};
  } else {
    return "";
  }
}

sub getinfo{
  my $self = shift;
  my $info = $self->{'info'};
  if (not defined($info)){
    my $domain = $self->{'domain'};
    my $owner = $self->{'owner'};
    my $repo = $self->{'repo'};
    my $url = "$domain/api/v4/projects/$owner%2F$repo";
    try{
      $info = decode_json(SCM::Registry::Get::curl($url));
    } catch ($v){
      $info = {};
    }
    $self->{'info'} = $info;
  }
  return $info;
}

sub gettags{
  my $self = shift;
  my $ret = $self->{'taglist'};
  if (not defined($ret)){
    my $id = $self->getinfo()->{'id'};
    my $domain = $self->{'domain'};
    my $url = "$domain/api/v4/projects/$id/repository/tags";
    try{
      $ret = decode_json(SCM::Registry::Get::curl($url));
    } catch ($v){
      $ret = [];
    }
    $self->{'taglist'} = $ret;
  }
  return $ret;
}

sub getlastcommiturl{
  my $self = shift;
  my $id = $self->getinfo()->{'id'};
  my $domain = $self->{'domain'};
  return "$domain/api/v4/projects/$id/repository/commits?per_page=1";
}

sub getcontenturl{
  my ($self, $path) = @_;
  my $domain = $self->{'domain'};
  my $owner = $self->{'owner'};
  my $repo = $self->{'repo'};
  my $commit = $self->getlastsha();
  return "$domain/$owner/$repo/-/raw/$commit/$path";
}

sub getlastcommit{
  my $self = shift;
  my $ret = $self->{'lastcommit'};
  if (not defined($ret)){
    my $url = $self->getlastcommiturl();
    try{
      $ret = decode_json(SCM::Registry::Get::curl($url))->[0];
    } catch ($v){
      $ret = {};
    }
    $self->{'lastcommit'} = $ret;

  }
  return $ret;
}

sub createrepo{
  my ($pkg, $url, $gitpath, $commit) = @_;
  my $url = URI->new($url);
  my $path = $url->path();
  my $origin = $url->host_port();
  my $scheme = $url->scheme();
  my @values = $path =~ '/+([^/]+)/+([^/]+?)(?:\.git)?$';
  my $github = {
   'owner' => $values[0],
   'repo' => $values[1],
   'commit' => $commit,
   'path' => $gitpath,
   'domain' => "$scheme://$origin"
  };
  bless($github, $pkg);
  return $github;
}

1;
