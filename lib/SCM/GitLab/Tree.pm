#!/usr/bin/perl

package SCM::GitLab::Tree;

#use Moose;
#extends
use base 'SCM::GitTree';

use SCM::Registry::Get;
use JSON::PP;
use SCM::GitLab::Repo;
use Scalar::Util 'blessed';
use experimental 'try';

sub new{
  my $pkg = shift;
  my $github = shift;
  if (not blessed($github)){
    if (not $github->isa('SCM::GitLab::Repo')){
      $github = SCM::GitHub::Repo->new($github, shift, shift, shift);
    }
  }
  my $ref = {'scm' => $github, 'dir' => {}};
  bless($ref, $pkg);
  return $ref;
}


#use Data::Dumper;

sub convert{
  my ($self, $hash) = @_;

#  print(Dumper($hash));

  my $type = $hash->{'type'};
  my $ref = {};
  my $pkg;
  my $github = $self->{'scm'};
  $ref->{'parent'} = $self;
  my $path = $hash->{'path'};

  if ($type eq 'tree'){
    goto ret2;
  }

  if ($type eq 'commit'){
    my $commit = $hash->{'id'};
    $github = $github->getsubmodule($path, $commit);
    return $github->getroottree();
  }

  if ($type eq 'blob'){
    $pkg = 'SCM::Blob';
    goto ret1;
  }

  goto ret3;
  ret2:
  $pkg = 'SCM::GitLab::Tree';
  $ref->{'dir'} = {};
  ret1:
  $ref->{'scm'} = $github;
  $ref->{'path'} = $path;
  bless($ref, $pkg);
  return $ref;
  ret3:
}

sub getlist{
  my $self = shift;
  my $list = $self->{'list'};
  if (not defined($list)){
    $list = {};
    $self->{'list'} = $list;
    my $scm = $self->getrepo();
    my $domain = $scm->{'domain'};
    my $id = $scm->getinfo()->{'id'};
    my $url = "$domain/api/v4/projects/$id/repository/tree";
    my $path = $self->{'path'};
    if ($path){
      $url = "$url?path=$path";
    }
    try{
      my $conf = SCM::Registry::Get::curl($url);
    #  print(Dumper($conf));
      $conf = decode_json($conf);
    #  print(Dumper($conf));
      for (@$conf){
        $list->{$_->{'name'}} = $_;
      }
    } catch ($v){
    }
  }
  return $list;
}

1;
