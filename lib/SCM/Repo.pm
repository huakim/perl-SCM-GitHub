package SCM::Repo;
#use Moose;


sub gettarballprefix{
}

sub getlastsha{
}

sub getlastshortsha{
  my $self = shift;
  my $sha = $self->getlastsha();
  if (length($sha) > 7){
    return substr($sha, 0, 7);
  } else {
    return $sha;
  }
}

sub gettarballurl{
}

sub getlasttagname{
}

sub getroottree{
}

1;
